# BoynerCase

### Getting Started

These instructions will help you to deploy and run as well as provide some insight on project's architecture.
In order to increase the ease of usage, a short video (mp4 format) can be found within `Video` folder.

### Used Technologies & 3rd Party Tools

- .NET Framework 4.6.1
- ASP.NET MVC5
- Ninject v3.3.3.0
- Log4net v2.0.8.0
- Newtonsoft.json v10.0.0.0
- Moq v4.8.0.0
- System.IO.Abstractions v2.1.0.178
- System.IO.Abstractions.TestingHelpers v2.1.0.178

### Assumptions

The usage of file data sources is not specifically indicated within the requirements. Therefore, I assumed that the sources are the same type of object like from a single table of one database. Then, I converted this info into one single manageable class object called `Item`.

### Running

From Visual Studio, project `Boyner.App.Website` must be set as **Startup Project**. Then, can simply be started (either with or without debugging) from Visual Studio. Also, there is a folder named as `published` which includes the published version of the website. This can also be put under an IIS Application Pool and then viewed if preferred.

### Logging

In this solution, I mainly used Error, Debug, and Info logs depending on the action. There certainly could be more logging but for this project, I'd like to believe that it is enough.
The logs can be found within the application's startup path inside a folder named as `Logs`.
There are 4 categories of logging level:

1. ProcessInfo
1. Debug
1. Error
1. Warning

Any of them can be activated or deactivated.
You can find logging setup in `Global.asax.cs` file as below:
```
SharpLogger.Start(2, LogType.ProcessInfo, LogType.Debug, LogType.Error, LogType.Warning);
```
The types are `params[]LogTypes`, so the logs can be reduced as reuqired.

### Solution's Brief Explanations

- **Boyner.Core.Logging**: Used for easy logging that depends on log4net.
- **Boyner.Core.Model**: This is the most important core project of the solution which includes Interfaces, Entities, Enums, some helpers, and extensions. No logic is implemented in this dll.
- **Boyner.App.Website**: MVC5 application and the startup project of the solution.
- **Boyner.Logic.File**: IO operation logics take place.
- **Boyner.Logic.Repository**: Content logics are handled here. Since there is no actual database, there is a cache repository which is fed by File operations in the higher layers.
- **Boyner.Test**: Unit tests are located here and currently around 40 tests implemented. According to ReSharper v2017.2 coverage results, these tests cover 78% of the whole solution and around 95% of the main projects (`Boyner.App.Website`, `Boyner.Logic.File`, and `Boyner.Logic.Repository`). For time purposes, I left `Boyner.Core.Logging` project out of test scope. Also, I left `Boyner.Core.Model` project as well since no logic is implemented.
