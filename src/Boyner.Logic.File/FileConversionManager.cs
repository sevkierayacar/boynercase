﻿using Boyner.Core.Model.Entities;
using Boyner.Core.Model.Enums;
using Boyner.Core.Model.Exceptions;
using Boyner.Core.Model.Extensions;
using Boyner.Core.Model.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO.Abstractions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Boyner.Core.Logging;

namespace Boyner.Logic.File
{
    public class FileConversionManager : IFileConversionManager
    {
        #region Fields

        private readonly IFileSystem _fileSystem;
        private readonly Encoding _encoding;

        #endregion

        #region Constructors

        public FileConversionManager(IFileSystem fileSystem)
        {
            _fileSystem = fileSystem;
            _encoding = Encoding.GetEncoding("ISO-8859-1");
        }

        #endregion

        #region Methods - Public - IFileToEntityConverter

        public async Task<List<Item>> ConvertToItemAsync(FileType fileType, string filePath)
        {
            SharpLogger.LogProcessInfo("Converting to items...", new { fileType, filePath});

            if (string.IsNullOrWhiteSpace(filePath))
            {
                throw new FileException("No fle path is found!");
            }
            try
            {
                switch (fileType)
                {
                    case FileType.Json:
                        return await ConvertToJsonAsync(filePath);
                    case FileType.Csv:
                        return await ConvertToCsvAsync(filePath);
                    case FileType.Xml:
                        return await ConvertToXmlAsync(filePath);
                }
            }
            catch (Exception ex)
            {
                throw new FileException("File parse error!", ex);
            }
            return new List<Item>();
        }

        #endregion

        #region Methods -Private

        private async Task<List<Item>> ConvertToXmlAsync(string filePath)
        {
            var result = new List<Item>();

            XmlDocument doc = new XmlDocument();

            await Task.Run(() =>
            {
                doc.LoadXml(_fileSystem.File.ReadAllText(filePath, _encoding).FixInvalidTurkishChars());
                var listNode = doc.SelectSingleNode("/ArrayOfXMLModel");
                if (listNode == null)
                {
                    throw new XmlException("Expected list node is not found!");
                }
                foreach (XmlNode node in listNode.ChildNodes)
                {
                    result.Add(new Item
                    {
                        ProductId = node.SelectSingleNode("Id")?.InnerText.Convert<long>() ?? 0,
                        VariantId = node.SelectSingleNode("VariantId")?.InnerText.Convert<long>() ?? 0,
                        ColorGroup = node.SelectSingleNode("GroupId")?.InnerText.Convert<long>() ?? 0,
                        Name = node.SelectSingleNode("Name")?.InnerText ?? string.Empty,
                        Price = node.SelectSingleNode("ListPrice")?.InnerText.Convert<decimal>() ?? 0.0M,
                        Barcode = node.SelectSingleNode("Barcode")?.InnerText ?? string.Empty,
                        Properties = node.SelectSingleNode("Properties")?.InnerText.Split(',').ToList() ??
                                     new List<string>()
                    });
                }
            });
            SharpLogger.LogDebug("Converting to xml finished!", new { filePath });
            return result;
        }
        private async Task<List<Item>> ConvertToJsonAsync(string filePath)
        {
            var result = new List<Item>();
            await Task.Run(() =>
            {
                result = _fileSystem.File.ReadAllText(filePath, _encoding)
                    .FixInvalidTurkishChars()
                    .FromJson<List<Item>>();

                if (result == null)
                {
                    throw new JsonReaderException("Json file cannot be parsed!");
                }
            });

            SharpLogger.LogDebug("Converting to json finished!", new { filePath });
            return result;
        }
        private async Task<List<Item>> ConvertToCsvAsync(string filePath)
        {
            var result = new List<Item>();
            await Task.Run(() =>
            {
                result = _fileSystem.File.ReadAllLines(filePath, _encoding)
                    .Select(c => FromCsv(c.FixInvalidTurkishChars()))
                    .ToList();

                if (!result.Any())
                {
                    throw new FileException("Csv file cannot be parsed!");
                }
            });
            SharpLogger.LogDebug("Converting to csv finished!", new { filePath });
            return result;
        }
        private Item FromCsv(string csvLine)
        {
            string[] values = csvLine.Split(';');
            return new Item
            {
                ProductId = values[0].Convert<long>(),
                VariantId = values[1].Convert<long>(),
                ColorGroup = values[2].Convert<long>(),
                Name = values[3].Convert<string>(),
                Barcode = values[4].Convert<string>(),
                Price = values[5].Replace(",", ".").Convert<decimal>(),
                Properties = values[6].Convert<string>().Split(',').ToList()
            };
        }

        #endregion
    }
}