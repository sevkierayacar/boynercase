﻿using Boyner.Core.Model.Enums;
using log4net;
using log4net.Config;
using log4net.Repository;
using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using Boyner.Core.Model.Extensions;

namespace Boyner.Core.Logging
{
    public static class SharpLogger
    {
        #region Fields

        private static int _stackFrameCount;
        private const int MinimumStackFrameCount = 0;
        private static readonly ILoggerRepository LoggerRepository = LogManager.GetRepository();

        #endregion

        #region Methods - Public

        /// <summary>
        /// Starts with default configuration which does not require an additional xml configuration file.
        /// As default, LogTypes are declared Error and ProcessInfo.
        /// </summary>
        public static void Start(int stackFrameCount = MinimumStackFrameCount, params LogType[] logTypes)
        {
            _stackFrameCount = stackFrameCount;

            if (!logTypes.Any())
                SharpLoggerSetup.Setup(LoggerRepository, LogType.Error, LogType.ProcessInfo);
            else
                SharpLoggerSetup.Setup(LoggerRepository, logTypes);
        }
        public static void Start(string url, bool isForceConfigure = false, int stackFrameCount = MinimumStackFrameCount)
        {
            _stackFrameCount = stackFrameCount >= MinimumStackFrameCount ? stackFrameCount : MinimumStackFrameCount;
            if (!LoggerRepository.Configured || isForceConfigure) //If its already configured, don't bother
                XmlConfigurator.ConfigureAndWatch(new System.IO.FileInfo(url));
        }

        #region Log Error

        public static void LogError(Exception ex, object customObj = null)
        {
            Log(LogType.Error, _stackFrameCount, GetExceptionMessage(ex).ToString(), customObj);
        }

        #endregion

        #region Log ProcessInfo

        public static void LogProcessInfo(string message, object customObj = null)
        {
            Log(LogType.ProcessInfo, _stackFrameCount, message, customObj);
        }

        #endregion

        #region Log Warning

        public static void LogWarning(string message, object customObj = null)
        {
            Log(LogType.Warning, _stackFrameCount, message, customObj);
        }

        #endregion

        #region Log Debug

        public static void LogDebug(string message, object customObj = null)
        {
            Log(LogType.Debug, _stackFrameCount, message, customObj);
        }

        #endregion

        #endregion

        #region Methods - Private

        private static void Log(LogType logType, int stackFrameCount, string message, params object[] customObjs)
        {
            ILog logger = LogManager.GetLogger(logType.ToString());

            if (logger != null)
            {
                StackFrame frame = new StackFrame(stackFrameCount);

                string msg = !string.IsNullOrWhiteSpace(message) ? "Message: " + message : null;
                string customObj = customObjs != null && customObjs.Any() ? "CustomObj: " + string.Join(" | ", customObjs.Select(c => c.ToJson())) : null;

                StringBuilder sb = new StringBuilder();
                sb.AppendLine(FormatMethodName(frame));
                sb.AppendLine("Ip: " + GetIpAddress());

                if (!string.IsNullOrWhiteSpace(msg))
                    sb.AppendLine(msg);
                if (!string.IsNullOrWhiteSpace(customObj))
                    sb.AppendLine(customObj);

                var sbString = sb.ToString();

                switch (logType)
                {
                    case LogType.Debug:
                        logger.Debug(sbString);
                        break;
                    case LogType.ProcessInfo:
                        logger.Info(sbString);
                        break;
                    case LogType.Error:
                        logger.Error(sbString);
                        break;
                    case LogType.Warning:
                        logger.Warn(sbString);
                        break;
                }
            }
        }
        private static StringBuilder GetExceptionMessage(Exception ex, StringBuilder message = null, int lineCount = 1)
        {
            if (message == null)
                message = new StringBuilder();

            message.AppendLine(
                "        " + lineCount + ") " +
                ex.GetType() +
                (!string.IsNullOrWhiteSpace(ex.Message) ? " - " + ex.Message : string.Empty) +
                (!string.IsNullOrWhiteSpace(ex.StackTrace) ? " - " + ex.StackTrace : string.Empty)
                );
            if (ex.InnerException != null)
            {
                return GetExceptionMessage(ex.InnerException, message, ++lineCount);
            }

            return message;
        }
        private static string FormatMethodName(StackFrame stackFrame)
        {
            MethodBase method = stackFrame.GetMethod();

            if (method != null)
            {
                Type declaringType = method.DeclaringType;

                if (declaringType != null)
                    return $"{declaringType.Namespace}.{declaringType.Name}.{method.Name}";
            }

            return $"{"NULL"}.{"NULL"}.{(method != null ? method.Name : "NULL")}";
        }
        private static string GetIpAddress()
        {
            var result = string.Empty;

            try
            {
                if (HttpContext.Current == null)
                    return "127.0.0.1";

                result = HttpContext.Current.Request.ServerVariables["HTTP_CLIENTIP"];

                if (!string.IsNullOrEmpty(result))
                {
                    string[] addresses = result.Split(',');
                    if (addresses.Length != 0)
                    {
                        result = addresses[0];
                    }
                }

                if (string.IsNullOrWhiteSpace(result))
                    result = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (!string.IsNullOrWhiteSpace(result))
                {
                    result = result.Split(',')[0];
                }

                if (string.IsNullOrWhiteSpace(result))
                    result = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

                if (result == "::1")
                    result = "127.0.0.1";
            }
            catch (System.Exception ex)
            {
                //nothing to throw required.
            }

            return result;
        }

        #endregion
    }
}
