﻿using Boyner.Core.Model.Enums;
using log4net;
using log4net.Appender;
using log4net.Core;
using log4net.Layout;
using log4net.Repository;
using log4net.Repository.Hierarchy;

namespace Boyner.Core.Logging
{
    public static class SharpLoggerSetup
    {
        #region Methods - Public

        public static void Setup(ILoggerRepository loggerRepository, params LogType[] logTypes)
        {
            Hierarchy hierarchy = (Hierarchy)loggerRepository;

            foreach (var logType in logTypes)
            {
                AddAppender(logType, CreateFileAppender(logType));
            }

            hierarchy.Root.Level = Level.All;
            hierarchy.Configured = true;
        }

        #endregion

        #region Methods - Private


        private static IAppender CreateFileAppender(LogType logType)
        {
            RollingFileAppender result = new RollingFileAppender
            {
                Name = $"{logType.ToString()}_FileAppender",
                File = $"Logs/{logType.ToString()}.txt",
                AppendToFile = true,
                MaxSizeRollBackups = 200,
                RollingStyle = RollingFileAppender.RollingMode.Composite,
                LockingModel = new FileAppender.MinimalLock()
            };

            //Result.Name = logType.ToEnumString();

            PatternLayout layout = new PatternLayout
            {
                ConversionPattern = "%date [%thread] %-5level %logger - %message%newline"
            };
            layout.ActivateOptions();

            result.Layout = layout;
            result.ActivateOptions();

            return result;
        }
        private static void AddAppender(LogType logType, IAppender appender)
        {
            var log = LogManager.GetLogger(logType.ToString());
            var l = (Logger)log.Logger;

            //l.Additivity = false;
            l.AddAppender(appender);
        }

        #endregion
    }
}
