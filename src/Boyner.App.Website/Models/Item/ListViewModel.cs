﻿using Boyner.App.Website.AppCode.Base;
using Boyner.Core.Model.Shared;
using System.Collections.Generic;

namespace Boyner.App.Website.Models.Item
{
    public class ListViewModel : ViewModelBase
    {
        #region Properties

        public string Search { get; set; }
        public Sort Sort { get; set; }
        public ICollection<Core.Model.Entities.Item> Items { get; set; }

        #endregion

        #region Constructors

        public ListViewModel()
        {
            Items = new List<Core.Model.Entities.Item>();
        }

        #endregion
    }
}