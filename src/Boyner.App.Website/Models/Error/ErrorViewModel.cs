﻿using Boyner.App.Website.AppCode.Base;
using Boyner.App.Website.AppCode.Enums;

namespace Boyner.App.Website.Models.Error
{
    public class ErrorViewModel : ViewModelBase
    {
        #region Properties

        public ErrorType ErrorType { get; set; }

        #endregion
    }
}