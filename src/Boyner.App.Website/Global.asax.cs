﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Boyner.App.Website.AppCode.Enums;
using Boyner.Core.Logging;
using Boyner.Core.Model.Enums;

namespace Boyner.App.Website
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            SharpLogger.Start(2, LogType.ProcessInfo, LogType.Debug, LogType.Error, LogType.Warning);
            SharpLogger.LogProcessInfo("App has started!");
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var ex = Server.GetLastError();

            SharpLogger.LogError(ex);

            try
            {
                ErrorType errTyp = ErrorType.UnknownError;

                try
                {
                    var exHttp = (HttpException) ex;
                    if (exHttp.GetHttpCode() == 404)
                        errTyp = ErrorType.PageNotFound;
                }
                catch (InvalidCastException)
                {
                    //No need to log. Can happen.
                }
                catch (Exception ex1)
                {
                    SharpLogger.LogError(ex1);
                }

                Server.ClearError();

                HttpContext.Current.Response.Redirect($"/Error/Index/{(int)errTyp}", true);
            }
            catch (Exception exNext)
            {
                SharpLogger.LogError(exNext);
            }
        }
        //For security purposes
        protected void Application_PreSendRequestHeaders()
        {
            if (!Request.IsLocal)
            {
                Response.Headers.Remove("Server");
                Response.Headers.Remove("X-AspNet-Version");
                Response.Headers.Remove("X-AspNetMvc-Version");
            }
        }
    }
}
