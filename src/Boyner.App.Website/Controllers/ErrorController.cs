﻿using Boyner.App.Website.AppCode.Enums;
using Boyner.App.Website.Models.Error;
using System.Web.Mvc;

namespace Boyner.App.Website.Controllers
{
    public class ErrorController : AppCode.Base.ControllerBase
    {
        #region MvcActions

        #region Index

        public ActionResult Index(int? id)
        {
            var model = new ErrorViewModel
            {
                ErrorType = id.HasValue ? (ErrorType)id.Value : ErrorType.UnknownError
            };

            switch (model.ErrorType)
            {
                case ErrorType.UnknownError:
                    model.Message = "Bilinmeyen bir hata oluştu!";
                    break;
                case ErrorType.PageNotFound:
                    model.Message = "Aradığınız sayfa bulunmamaktadır.";
                    break;
            }

            return View("Index", base.LayoutDefault, model);
        }

        #endregion

        #endregion
    }
}