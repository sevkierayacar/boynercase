﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Boyner.App.Website.AppCode.ActionFilters;
using Boyner.App.Website.Models.Item;
using Boyner.Core.Model.Interfaces;
using System.Web.Mvc;
using Boyner.Core.Logging;
using Boyner.Core.Model.Exceptions;

namespace Boyner.App.Website.Controllers
{
    [RequireSource]
    public class ItemController : AppCode.Base.ControllerBase
    {
        #region Fields

        private readonly IItemRepository _itemRepository;

        #endregion

        #region Constructors

        public ItemController(IItemRepository itemRepository)
        {
            _itemRepository = itemRepository;
        }

        #endregion

        #region Mvc Actions

        #region List

        [HttpGet]
        public async Task<ActionResult> List()
        {
            var model = new ListViewModel();

            try
            {
                model.Items = await _itemRepository.AllAsync();
                SharpLogger.LogDebug("All items are requested!", new { model.Items.Count });
            }
            catch (RepositoryException ex)
            {
                //Its a known expected exception. Just log it and show a meaningful message
                SharpLogger.LogError(ex);
                model.Message = "Tüm liste alırken hata oluştu!";
            }
            catch (Exception ex)
            {
                //Something really went wrong that we don't know of
                throw new ControllerException("Item Controller failed!", ex);
            }

            return View("List", base.LayoutDefault, model);
        }
        [HttpPost]
        public async Task<ActionResult> List(ListViewModel model)
        {
            try
            {
                model.Items = !string.IsNullOrWhiteSpace(model.Search)
                    ? await _itemRepository.FilterAsync(model.Search, model.Sort)
                    : await _itemRepository.AllAsync(model.Sort);

                SharpLogger.LogDebug("Fitered items are requested!", new { model.Search, model.Sort?.SortingDirectionType, model.Sort?.SortingField, model.Items.Count });
            }
            catch (RepositoryException ex)
            { 
                //Its a known expected exception. Just log it and show a meaningful message
                SharpLogger.LogError(ex);
                model.Message = "Filtrelenmiş liste alırken hata oluştu!";
            }
            catch (Exception ex)
            { 
                //Something really went wrong that we don't know of
                throw new ControllerException("Item Controller failed!", ex);
            }

            return View("List", base.LayoutDefault, model);
        }

        #endregion

        #endregion
    }
}