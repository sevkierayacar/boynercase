﻿using System;
using Boyner.Core.Model.Entities;
using Boyner.Core.Model.Enums;
using Boyner.Core.Model.Interfaces;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Web.Mvc;
using Boyner.App.Website.AppCode.Base;
using Boyner.Core.Logging;
using Boyner.Core.Model.Exceptions;

namespace Boyner.App.Website.Controllers
{
    public class SourceController : AppCode.Base.ControllerBase
    {
        #region Fields

        private readonly IFileConversionManager _fileToEntityConverter;
        private readonly IItemRepository _itemRepository;
        private readonly IHttpRunTimeHelper _httpRunTimeHelper;

        #endregion

        #region Constructors

        public SourceController(IFileConversionManager fileToEntityConverter, IItemRepository itemRepository, IHttpRunTimeHelper httpRunTimeHelper)
        {
            _fileToEntityConverter = fileToEntityConverter;
            _itemRepository = itemRepository;
            _httpRunTimeHelper = httpRunTimeHelper;
        }

        #endregion

        #region MvcActions

        #region Load

        [HttpGet]
        public async Task<ActionResult> Load()
        {
            var items = new List<Item>();
            var model = new ViewModelBase();

            try
            {
                if (!_itemRepository.HasInitialized)
                {
                    SharpLogger.LogProcessInfo("Source has not been initialized! Getting source started...");

                    foreach (FileType fileType in Enum.GetValues(typeof(FileType)))
                    {
                        items.AddRange(await _fileToEntityConverter
                            .ConvertToItemAsync(
                                fileType,
                                Path.Combine(_httpRunTimeHelper.AppDomainAppPath, "assets", "data", $"source.{fileType.ToString()}")
                            )
                            .ConfigureAwait(false));
                    }

                    SharpLogger.LogProcessInfo("Getting source completed!");
                }

                _itemRepository.Initialize(items);

            }
            catch (FileException ex)
            {
                //Its a known expected exception. Just log it and show a meaningful message
                SharpLogger.LogError(ex);
                model.Message = "Dosyalar okunurken bir hata oluştu!";
                return View("Index", base.LayoutDefault, model);
            }
            catch (RepositoryException ex)
            {
                //Its a known expected exception. Just log it and show a meaningful message
                SharpLogger.LogError(ex);
                model.Message = "Okunan dosyaları cache'e yazarken hata oluştu!";
                return View("Index", base.LayoutDefault, model);
            }
            catch (Exception ex)
            {
                //Something really went wrong that we don't know of
                throw new ControllerException("Source Controller failed!", ex);
            }
            return RedirectToAction("List", "Item");
        }

        #endregion

        #endregion
    }
}