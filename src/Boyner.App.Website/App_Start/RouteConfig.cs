﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Boyner.App.Website
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Source", action = "Load", id = UrlParameter.Optional }
            );
        }
    }
}