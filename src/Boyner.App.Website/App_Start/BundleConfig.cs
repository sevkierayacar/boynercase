﻿using System.Web.Optimization;

namespace Boyner.App.Website
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Assets/css").Include(
                "~/Assets/css/style.css"
            ));

            bundles.Add(new ScriptBundle("~/Assets/js").Include(
                "~/Assets/js/jquery-3.2.1.min.js"
            ));
        }
    }
}