﻿namespace Boyner.App.Website.AppCode.Enums
{
    public enum ErrorType
    {
        UnknownError = 1,
        PageNotFound = 2
    }
}