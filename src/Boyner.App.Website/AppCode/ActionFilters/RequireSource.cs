﻿using Boyner.Core.Model.Interfaces;
using System.Web.Mvc;
using System.Web.Routing;
using Boyner.Core.Logging;

namespace Boyner.App.Website.AppCode.ActionFilters
{
    /// <summary>
    /// Use this when the contoller or action absolutely requires source. (ex. error controller does not require)
    /// </summary>
    public class RequireSource : ActionFilterAttribute
    {
        #region Fields

        private readonly IItemRepository _itemRepository;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor for actual use
        /// </summary>
        public RequireSource()
        {
            _itemRepository = (IItemRepository)DependencyResolver.Current.GetService(typeof(IItemRepository));
        }

        /// <summary>
        /// Constructor for testing purposes
        /// </summary>
        /// <param name="itemRepository"></param>
        public RequireSource(IItemRepository itemRepository)
        {
            _itemRepository = itemRepository;
        }

        #endregion

        #region Methods - Public

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!_itemRepository.HasInitialized)
            {
                SharpLogger.LogProcessInfo("A page accessed when there is no source");

                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Source",
                    action = "Load"
                }));
            }

            base.OnActionExecuting(filterContext);
        }

        #endregion
    }
}