﻿using System.Web.Mvc;

namespace Boyner.App.Website.AppCode.Base
{
    public class ControllerBase : Controller
    {
        #region Properties

        public string LayoutDefault => "~/Views/Shared/_LayoutDefault.cshtml";

        #endregion
    }
}