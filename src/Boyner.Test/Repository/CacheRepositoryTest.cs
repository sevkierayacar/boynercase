﻿using System.Collections.Generic;
using Boyner.Core.Model.Entities;
using Boyner.Core.Model.Interfaces;
using Boyner.Logic.Repository;
using Boyner.Test.Base;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Boyner.Test.Repository
{
    [TestClass]
    public class CacheRepositoryTest : TestBase
    {
        #region Fields

        private ICacheRepository<List<Item>> _cacheRepository;

        #endregion

        #region Initialize

        [TestInitialize]
        public void Initialize()
        {
            _cacheRepository = new CacheRepository<List<Item>>();
        }

        #endregion

        #region Tests

        [TestMethod]
        public void CacheRepository_WhenCacheHasNoItem()
        {
            #region Asserts

            Assert.IsFalse(_cacheRepository.HasCache);

            #endregion
        }
        [TestMethod]
        public void CacheRepository_AddItem_Success()
        {
            #region Act

            _cacheRepository.Add(base.Items);

            #endregion

            #region Asserts

            Assert.IsTrue(_cacheRepository.HasCache);
            Assert.IsNotNull(_cacheRepository.CacheObject);
            Assert.AreEqual(base.Items.Count, _cacheRepository.CacheObject.Count);

            #endregion
        }
        [TestMethod]
        public void CacheRepository_RemoveItem_Success()
        {
            #region Act

            _cacheRepository.Remove();

            #endregion

            #region Asserts

            Assert.IsFalse(_cacheRepository.HasCache);
            Assert.IsNull(_cacheRepository.CacheObject);

            #endregion
        }

        #endregion
    }
}