﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Boyner.Core.Model.Entities;
using Boyner.Core.Model.Exceptions;
using Boyner.Core.Model.Interfaces;
using Boyner.Logic.Repository;
using Boyner.Test.Base;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Boyner.Test.Repository
{
    [TestClass]
    public class ItemRepositoryTest : TestBase
    {
        #region Fields

        private IItemRepository _itemRepository;
        private Mock<ICacheRepository<List<Item>>> _mockCacheRepository;

        #endregion

        #region Initialize

        [TestInitialize]
        public void Initialize()
        {
            _mockCacheRepository = new Mock<ICacheRepository<List<Item>>>();
            _itemRepository = new ItemRepository(_mockCacheRepository.Object);
        }

        #endregion

        #region Tests

        #region Initialize

        [TestMethod]
        public void ItemRepository_Initialize_Success()
        {
            #region Setups

            _mockCacheRepository.SetupGet(c => c.HasCache).Returns(true);

            #endregion

            #region Act

            var result = _itemRepository.HasInitialized;

            #endregion

            #region Asserts

            Assert.IsTrue(result);

            #endregion
        }

        [TestMethod]
        public void ItemRepository_Initialize_WhenNothingLoaded_Success()
        {
            #region Setups

            _mockCacheRepository.SetupGet(c => c.CacheObject).Returns(base.Items);
            _mockCacheRepository.SetupSequence(c => c.HasCache).Returns(false).Returns(true);

            #endregion

            #region Act

            _itemRepository.Initialize(It.IsAny<List<Item>>());

            #endregion

            #region Asserts
            
            _mockCacheRepository.VerifyGet(c => c.HasCache, Times.AtLeastOnce);
            _mockCacheRepository.Verify(c => c.Add(It.IsAny<List<Item>>()), Times.Once);
            _mockCacheRepository.Verify(c => c.Remove(), Times.Never);

            #endregion
        }
        [TestMethod]
        [ExpectedException(typeof(RepositoryException), "Cache is not filled!")]
        public void ItemRepository_Initialize_WhenNothingLoaded_Fail()
        {
            #region Setups

            _mockCacheRepository.SetupGet(c => c.CacheObject).Returns(base.Items);
            _mockCacheRepository.SetupSequence(c => c.HasCache).Returns(false).Returns(false);

            #endregion

            #region Act

            _itemRepository.Initialize(It.IsAny<List<Item>>());

            #endregion

            #region Asserts

            _mockCacheRepository.VerifyGet(c => c.HasCache, Times.AtLeastOnce);
            _mockCacheRepository.Verify(c => c.Add(It.IsAny<List<Item>>()), Times.Once);
            _mockCacheRepository.Verify(c => c.Remove(), Times.Never);

            #endregion
        }
        [TestMethod]
        public void ItemRepository_Initialize_WhenAlreadyLoaded_Success()
        {
            #region Setups

            _mockCacheRepository.SetupGet(c => c.CacheObject).Returns(base.Items);
            _mockCacheRepository.SetupSequence(c => c.HasCache).Returns(true).Returns(true);

            #endregion

            #region Act

            _itemRepository.Initialize(It.IsAny<List<Item>>());

            #endregion

            #region Asserts
            
            _mockCacheRepository.VerifyGet(c => c.HasCache, Times.AtLeastOnce);
            _mockCacheRepository.Verify(c => c.Add(It.IsAny<List<Item>>()), Times.Once);
            _mockCacheRepository.Verify(c => c.Remove(), Times.Once);

            #endregion
        }
        [TestMethod]
        [ExpectedException(typeof(RepositoryException), "Cache is not filled!")]
        public void ItemRepository_Initialize_WhenAlreadyLoaded_Fail()
        {
            #region Setups

            _mockCacheRepository.SetupGet(c => c.CacheObject).Returns(base.Items);
            _mockCacheRepository.SetupSequence(c => c.HasCache).Returns(true).Returns(false);

            #endregion

            #region Act

            _itemRepository.Initialize(It.IsAny<List<Item>>());

            #endregion

            #region Asserts

            _mockCacheRepository.VerifyGet(c => c.HasCache, Times.AtLeastOnce);
            _mockCacheRepository.Verify(c => c.Add(It.IsAny<List<Item>>()), Times.Once);
            _mockCacheRepository.Verify(c => c.Remove(), Times.Once);

            #endregion
        }

        #endregion

        #region All

        [TestMethod]
        public async Task ItemRepository_All_Success()
        {
            #region Setups

            _mockCacheRepository.SetupGet(c => c.CacheObject).Returns(base.Items);
            _mockCacheRepository.SetupSequence(c => c.HasCache).Returns(true);

            #endregion

            #region Act

            var result = await _itemRepository.AllAsync();

            #endregion

            #region Asserts

            Assert.AreEqual(base.Items.Count, result.Count());
            _mockCacheRepository.VerifyGet(c => c.HasCache, Times.Once);
            _mockCacheRepository.Verify(c => c.Add(It.IsAny<List<Item>>()), Times.Never);
            _mockCacheRepository.Verify(c => c.Remove(), Times.Never);

            #endregion
        }
        [TestMethod]
        [ExpectedException(typeof(RepositoryException), "Items have not been loaded yet!")]
        public async Task ItemRepository_All_Fail()
        {
            #region Setups

            _mockCacheRepository.SetupGet(c => c.CacheObject).Returns(base.Items);
            _mockCacheRepository.SetupSequence(c => c.HasCache).Returns(false);

            #endregion

            #region Act

            await _itemRepository.AllAsync();

            #endregion
        }

        #endregion

        #region Filter

        [TestMethod]
        public async Task ItemRepository_Filter_Success_With_MayContain()
        {
            #region Setups

            _mockCacheRepository.SetupGet(c => c.CacheObject).Returns(base.Items);
            _mockCacheRepository.SetupSequence(c => c.HasCache).Returns(true);

            #endregion

            #region Act

            var result = await _itemRepository.FilterAsync("beyaz ceket");

            #endregion

            #region Asserts

            Assert.IsNotNull(result);
            Assert.AreEqual(22, result.Count);
            Assert.IsTrue(result.All
                (
                    p =>
                        (
                            (p.Properties.Any(c => c.IndexOf("beyaz", StringComparison.OrdinalIgnoreCase) >= 0))
                            || p.Properties.Any(c => c.IndexOf("ceket", StringComparison.OrdinalIgnoreCase) >= 0)
                        )
                        ||
                        (
                            (p.Name.IndexOf("beyaz", StringComparison.OrdinalIgnoreCase) >= 0)
                            || p.Name.IndexOf("ceket", StringComparison.OrdinalIgnoreCase) >= 0)
                )
            );

            var first = result.First();
            Assert.AreEqual("8680954677255", first.Barcode);
            Assert.AreEqual(2114292, first.ColorGroup);
            Assert.AreEqual("Klasik Gömlek", first.Name);
            Assert.AreEqual(129.0M, first.Price);
            Assert.AreEqual(2809882, first.ProductId);
            Assert.AreEqual(15002531, first.VariantId);
            Assert.AreEqual(3, first.Properties.Count);
            Assert.IsTrue(first.Properties.Any(c => c.IndexOf("beyaz", StringComparison.OrdinalIgnoreCase) >= 0));

            #endregion
        }
        [TestMethod]
        public async Task ItemRepository_Filter_Success_With_AllContain()
        {
            #region Setups

            _mockCacheRepository.SetupGet(c => c.CacheObject).Returns(base.Items);
            _mockCacheRepository.SetupSequence(c => c.HasCache).Returns(true);

            #endregion

            #region Act

            var result = await _itemRepository.FilterAsync("\"beyaz ceket\"");

            #endregion

            #region Asserts

            Assert.IsNotNull(result);
            Assert.AreEqual(6, result.Count);
            Assert.IsTrue(result.All
                (
                    p => 
                           (
                               (p.Properties.Any(c => c.IndexOf("beyaz", StringComparison.OrdinalIgnoreCase) >= 0))
                               || p.Properties.Any(c => c.IndexOf("ceket", StringComparison.OrdinalIgnoreCase) >= 0)
                           )
                           && 
                           (
                               (p.Name.IndexOf("beyaz", StringComparison.OrdinalIgnoreCase) >= 0)
                               || p.Name.IndexOf("ceket", StringComparison.OrdinalIgnoreCase) >= 0)
                           )
                    );

            var first = result.First();
            Assert.AreEqual("DL-I-8-DL-I-5-869987", first.Barcode);
            Assert.AreEqual(1369498, first.ColorGroup);
            Assert.AreEqual("Klasik Ceket", first.Name);
            Assert.AreEqual(199.99M, first.Price);
            Assert.AreEqual(2313960, first.ProductId);
            Assert.AreEqual(8645116, first.VariantId);
            Assert.AreEqual(3, first.Properties.Count);
            Assert.IsTrue(first.Properties.Any(c => c.IndexOf("beyaz", StringComparison.OrdinalIgnoreCase) >= 0));

            #endregion
        }
        [TestMethod]
        public async Task ItemRepository_Filter_Success_With_MustContain()
        {
            #region Setups

            _mockCacheRepository.SetupGet(c => c.CacheObject).Returns(base.Items);
            _mockCacheRepository.SetupSequence(c => c.HasCache).Returns(true);

            #endregion

            #region Act

            var result = await _itemRepository.FilterAsync("klasik siyah+");

            #endregion

            #region Asserts

            Assert.IsNotNull(result);
            Assert.AreEqual(6, result.Count);
            Assert.IsTrue(result.All(p => p.Properties.Any(c => c.IndexOf("siyah", StringComparison.OrdinalIgnoreCase) >= 0)));

            var first = result.First();
            Assert.AreEqual("8680170019945", first.Barcode);
            Assert.AreEqual(2751303, first.ColorGroup);
            Assert.AreEqual("Klasik Ceket", first.Name);
            Assert.AreEqual(399.99M, first.Price);
            Assert.AreEqual(2313960, first.ProductId);
            Assert.AreEqual(15581322, first.VariantId);
            Assert.AreEqual(3, first.Properties.Count);

            #endregion
        }
        [TestMethod]
        public async Task ItemRepository_Filter_Success_With_NotContain()
        {
            #region Setups

            _mockCacheRepository.SetupGet(c => c.CacheObject).Returns(base.Items);
            _mockCacheRepository.SetupSequence(c => c.HasCache).Returns(true);

            #endregion

            #region Act

            var result = await _itemRepository.FilterAsync("gömlek siyah-");

            #endregion

            #region Asserts

            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            Assert.IsTrue(result.All(p => p.Properties.All(c => c.IndexOf("siyah", StringComparison.OrdinalIgnoreCase) < 0)));

            var first = result.First();
            Assert.AreEqual("8680954677255", first.Barcode);
            Assert.AreEqual(2114292, first.ColorGroup);
            Assert.AreEqual("Klasik Gömlek", first.Name);
            Assert.AreEqual(129M, first.Price);
            Assert.AreEqual(2809882, first.ProductId);
            Assert.AreEqual(15002531, first.VariantId);
            Assert.AreEqual(3, first.Properties.Count);

            #endregion
        }

        #endregion

        #endregion
    }
}