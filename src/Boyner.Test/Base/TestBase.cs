﻿using Boyner.Core.Model.Entities;
using System.Collections.Generic;

namespace Boyner.Test.Base
{
    public class TestBase
    {
        #region Properties

        public List<Item> Items { get; }

        #endregion

        #region Constructors

        public TestBase()
        {
            Items = new List<Item>
            {
                new Item
                {
                    ProductId = 2809882,
                    VariantId = 15002531,
                    ColorGroup = 2114292,
                    Name = "Klasik Gömlek",
                    Barcode = "8680954677255",
                    Price = 129.0000M,
                    Properties = new List<string> {"G051GL004.ENNA.134390 Gömlek", "Beyaz", "45"}
                },
                new Item
                {
                    ProductId = 2313960,
                    VariantId = 15581322,
                    ColorGroup = 2751303,
                    Name = "Klasik Ceket",
                    Barcode = "8680170019945",
                    Price = 399.9900M,
                    Properties = new List<string> {"G051KS002.P44.L03001 Ceket", "Siyah.", "56-6N"}
                },
                new Item
                {
                    ProductId = 2809882,
                    VariantId = 15002528,
                    ColorGroup = 2114292,
                    Name = "Klasik Gömlek",
                    Barcode = "8680954677279",
                    Price = 99.9900M,
                    Properties = new List<string> {"G051GL004.ENNA.134390 Gömlek", "Beyaz", "42"}
                },
                new Item
                {
                    ProductId = 2809882,
                    VariantId = 15002523,
                    ColorGroup = 2114292,
                    Name = "Klasik Gömlek",
                    Barcode = "8680954677224",
                    Price = 129.0000M,
                    Properties = new List<string> {"G051GL004.ENNA.134390 Gömlek", "Beyaz", "37"}
                },
                new Item
                {
                    ProductId = 2313960,
                    VariantId = 8645116,
                    ColorGroup = 1369498,
                    Name = "Klasik Ceket",
                    Barcode = "DL-I-8-DL-I-5-869987",
                    Price = 199.9900M,
                    Properties = new List<string> {"G051KS002.P44.L03001 Ceket", "Beyaz", "50-6N"}
                },
                new Item
                {
                    ProductId = 2313960,
                    VariantId = 8645122,
                    ColorGroup = 1369498,
                    Name = "Klasik Ceket",
                    Barcode = "DL-I-7-DL-I-6-869987",
                    Price = 199.9900M,
                    Properties = new List<string> {"G051KS002.P44.L03001 Ceket", "Beyaz", "52-6N"}
                },
                new Item
                {
                    ProductId = 2809882,
                    VariantId = 15002529,
                    ColorGroup = 2114292,
                    Name = "Klasik Gömlek",
                    Barcode = "8680954677286",
                    Price = 129.0000M,
                    Properties = new List<string> {"G051GL004.ENNA.134390 Gömlek", "Beyaz", "43"}
                },
                new Item
                {
                    ProductId = 2963741,
                    VariantId = 15581462,
                    ColorGroup = 2794224,
                    Name = "Klasik Ceket",
                    Barcode = "8680533528725",
                    Price = 199.9900M,
                    Properties = new List<string> {"G051KS002.IT2.G04060 Ceket", "Lacivert Lacivert", "48-6N"}
                },
                new Item
                {
                    ProductId = 3493388,
                    VariantId = 17946828,
                    ColorGroup = 2969926,
                    Name = "Gömlek",
                    Barcode = "8699871281926",
                    Price = 49.9500M,
                    Properties = new List<string> {"S3K16C010041 Gömlek Burgundy Navy", "Burgundy Navy", "M"}
                },
                new Item
                {
                    ProductId = 2963743,
                    VariantId = 15581472,
                    ColorGroup = 2794225,
                    Name = "Klasik Ceket",
                    Barcode = "4000013067942",
                    Price = 399.9900M,
                    Properties = new List<string> {"G051KS002.NW9.G04060 Ceket", "Siyah Siyah", "54-6N"}
                },
                new Item
                {
                    ProductId = 2313960,
                    VariantId = 15581283,
                    ColorGroup = 2751303,
                    Name = "Klasik Ceket",
                    Barcode = "8680170019921",
                    Price = 399.9900M,
                    Properties = new List<string> {"G051KS002.P44.L03001 Ceket", "Siyah.", "52-6N"}
                },
                new Item
                {
                    ProductId = 2809882,
                    VariantId = 15002530,
                    ColorGroup = 2114292,
                    Name = "Klasik Gömlek",
                    Barcode = "8680954677200",
                    Price = 129.0000M,
                    Properties = new List<string> {"G051GL004.ENNA.134390 Gömlek", "Beyaz", "44"}
                },
                new Item
                {
                    ProductId = 2313960,
                    VariantId = 18395213,
                    ColorGroup = 1369498,
                    Name = "Klasik Ceket",
                    Barcode = "8699870371352",
                    Price = 399.0000M,
                    Properties = new List<string> {"G051KS002.P44.L03001 Ceket", "Beyaz", "62-6N"}
                },
                new Item
                {
                    ProductId = 2809882,
                    VariantId = 15002526,
                    ColorGroup = 2114292,
                    Name = "Klasik Gömlek",
                    Barcode = "8680954677217",
                    Price = 99.9900M,
                    Properties = new List<string> {"G051GL004.ENNA.134390 Gömlek", "Beyaz", "40"}
                },
                new Item
                {
                    ProductId = 3493388,
                    VariantId = 17946830,
                    ColorGroup = 2969926,
                    Name = "Gömlek",
                    Barcode = "8699871281940",
                    Price = 49.9500M,
                    Properties = new List<string> {"S3K16C010041 Gömlek Burgundy Navy", "Burgundy Navy", "XL"}
                },
                new Item
                {
                    ProductId = 2809882,
                    VariantId = 15002525,
                    ColorGroup = 2114292,
                    Name = "Klasik Gömlek",
                    Barcode = "8680954677231",
                    Price = 99.9900M,
                    Properties = new List<string> {"G051GL004.ENNA.134390 Gömlek", "Beyaz", "39"}
                },
                new Item
                {
                    ProductId = 2809882,
                    VariantId = 15002524,
                    ColorGroup = 2114292,
                    Name = "Klasik Gömlek",
                    Barcode = "8680954677248",
                    Price = 99.9900M,
                    Properties = new List<string> {"G051GL004.ENNA.134390 Gömlek", "Beyaz", "38"}
                },
                new Item
                {
                    ProductId = 2313960,
                    VariantId = 15581477,
                    ColorGroup = 2751303,
                    Name = "Klasik Ceket",
                    Barcode = "8680170019952",
                    Price = 199.9900M,
                    Properties = new List<string> {"G051KS002.P44.L03001 Ceket", "Siyah.", "58-6N"}
                },
                new Item
                {
                    ProductId = 2313960,
                    VariantId = 8645256,
                    ColorGroup = 1369498,
                    Name = "Klasik Ceket",
                    Barcode = "8699870371345",
                    Price = 199.9900M,
                    Properties = new List<string> {"G051KS002.P44.L03001 Ceket", "Beyaz", "60-6N"}
                },
                new Item
                {
                    ProductId = 2963743,
                    VariantId = 15581471,
                    ColorGroup = 2794225,
                    Name = "Klasik Ceket",
                    Barcode = "4000013067966",
                    Price = 399.9900M,
                    Properties = new List<string> {"G051KS002.NW9.G04060 Ceket", "Siyah Siyah", "48-6N"}
                },
                new Item
                {
                    ProductId = 2809882,
                    VariantId = 15002527,
                    ColorGroup = 2114292,
                    Name = "Klasik Gömlek",
                    Barcode = "8680954677262",
                    Price = 99.9900M,
                    Properties = new List<string> {"G051GL004.ENNA.134390 Gömlek", "Beyaz", "41"}
                },
                new Item
                {
                    ProductId = 2313960,
                    VariantId = 8645129,
                    ColorGroup = 1369498,
                    Name = "Klasik Ceket",
                    Barcode = "DL-I-6-DL-I-7-869987",
                    Price = 199.9900M,
                    Properties = new List<string> {"G051KS002.P44.L03001 Ceket", "Beyaz", "54-6N"}
                },
                new Item
                {
                    ProductId = 2313960,
                    VariantId = 8645135,
                    ColorGroup = 1369498,
                    Name = "Klasik Ceket",
                    Barcode = "DL-I-4-DL-I-8-869987",
                    Price = 199.9900M,
                    Properties = new List<string> {"G051KS002.P44.L03001 Ceket", "Beyaz", "56-6N"}
                },
                new Item
                {
                    ProductId = 3493388,
                    VariantId = 17946827,
                    ColorGroup = 2969926,
                    Name = "Gömlek",
                    Barcode = "8699871281919",
                    Price = 49.9500M,
                    Properties = new List<string> {"S3K16C010041 Gömlek Burgundy Navy", "Burgundy Navy", "L"}
                },
                new Item
                {
                    ProductId = 2963743,
                    VariantId = 15581470,
                    ColorGroup = 2794225,
                    Name = "Klasik Ceket",
                    Barcode = "4000013067973",
                    Price = 399.9900M,
                    Properties = new List<string> {"G051KS002.NW9.G04060 Ceket", "Siyah Siyah", "46-6N"}
                },

            };
        }

        #endregion
    }
}