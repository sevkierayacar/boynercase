﻿using Boyner.App.Website.Controllers;
using Boyner.Core.Model.Entities;
using Boyner.Core.Model.Enums;
using Boyner.Core.Model.Interfaces;
using Boyner.Test.Base;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using Boyner.App.Website.AppCode.Base;
using Boyner.Core.Model.Exceptions;

namespace Boyner.Test.Contoller
{
    [TestClass]
    public class SourceControllerTest : TestBase
    {
        #region Fields

        private SourceController _sourceController;
        private Mock<IItemRepository> _mockItemRepository;
        private Mock<IFileConversionManager> _mockFileConversionManager;
        private Mock<IHttpRunTimeHelper> _mockHttpRunTimeHelper;

        #endregion

        #region Initialize

        [TestInitialize]
        public void Initialize()
        {
            _mockItemRepository = new Mock<IItemRepository>();
            _mockFileConversionManager = new Mock<IFileConversionManager>();
            _mockHttpRunTimeHelper = new Mock<IHttpRunTimeHelper>();
            _sourceController = new SourceController(_mockFileConversionManager.Object, _mockItemRepository.Object, _mockHttpRunTimeHelper.Object);
        }

        #endregion

        #region Tests

        [TestMethod]
        public async Task SourceController_Load_HttpGet_WhenInitialized_Success()
        {
            #region Setups

            _mockItemRepository.SetupGet(c => c.HasInitialized).Returns(true);
            _mockItemRepository.Setup(c => c.Initialize(It.IsAny<List<Item>>()));
            _mockFileConversionManager.Setup(c => c.ConvertToItemAsync(It.IsAny<FileType>(), It.IsAny<string>())).ReturnsAsync(base.Items);
            _mockHttpRunTimeHelper.Setup(c => c.AppDomainAppPath).Returns("somepath");

            #endregion

            #region Acts

            var result = await _sourceController.Load() as RedirectToRouteResult;

            #endregion

            #region Asserts

            Assert.IsNotNull(result);
            Assert.AreEqual("Item", result.RouteValues["controller"]);
            Assert.AreEqual("List", result.RouteValues["action"]);

            _mockItemRepository.VerifyGet(c => c.HasInitialized, Times.Once);
            _mockItemRepository.Verify(c => c.Initialize(It.IsAny<List<Item>>()), Times.Once);
            _mockFileConversionManager.Verify(c => c.ConvertToItemAsync(It.IsAny<FileType>(), It.IsAny<string>()), Times.Never);

            #endregion
        }
        [TestMethod]
        public async Task SourceController_Load_HttpGet_WhenNotInitialized_Success()
        {
            #region Setups

            _mockItemRepository.SetupGet(c => c.HasInitialized).Returns(false);
            _mockItemRepository.Setup(c => c.Initialize(It.IsAny<List<Item>>()));
            _mockFileConversionManager.Setup(c => c.ConvertToItemAsync(It.IsAny<FileType>(), It.IsAny<string>())).ReturnsAsync(base.Items);
            _mockHttpRunTimeHelper.Setup(c => c.AppDomainAppPath).Returns("somepath");

            #endregion

            #region Acts

            var result = await _sourceController.Load() as RedirectToRouteResult;

            #endregion

            #region Asserts

            Assert.IsNotNull(result);
            Assert.AreEqual("Item", result.RouteValues["controller"]);
            Assert.AreEqual("List", result.RouteValues["action"]);

            _mockItemRepository.VerifyGet(c => c.HasInitialized, Times.Once);
            _mockItemRepository.Verify(c => c.Initialize(It.IsAny<List<Item>>()), Times.Once);
            _mockFileConversionManager.Verify(c => c.ConvertToItemAsync(It.IsAny<FileType>(), It.IsAny<string>()), Times.Exactly(Enum.GetValues(typeof(FileType)).Length));

            #endregion
        }
        [TestMethod]
        public async Task SourceController_Load_HttpGet_RepositoryFail()
        {
            #region Setups

            _mockItemRepository.SetupGet(c => c.HasInitialized).Returns(true);
            _mockItemRepository.Setup(c => c.Initialize(It.IsAny<List<Item>>())).Throws<RepositoryException>();
            _mockFileConversionManager.Setup(c => c.ConvertToItemAsync(It.IsAny<FileType>(), It.IsAny<string>())).ReturnsAsync(base.Items);
            _mockHttpRunTimeHelper.Setup(c => c.AppDomainAppPath).Returns("somepath");

            #endregion

            #region Acts

            var result = await _sourceController.Load() as ViewResult;

            #endregion

            #region Asserts

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Model);
            Assert.AreEqual("Index", result.ViewName);
            Assert.AreEqual(_sourceController.LayoutDefault, result.MasterName);
            Assert.IsInstanceOfType(result.Model, typeof(ViewModelBase));

            var viewModel = (ViewModelBase)result.Model;
            Assert.IsNotNull(viewModel);
            Assert.IsNotNull(viewModel.Message);
            Assert.AreEqual("Okunan dosyaları cache'e yazarken hata oluştu!", viewModel.Message);

            _mockItemRepository.VerifyGet(c => c.HasInitialized, Times.Once);
            _mockItemRepository.Verify(c => c.Initialize(It.IsAny<List<Item>>()), Times.Once);
            _mockFileConversionManager.Verify(c => c.ConvertToItemAsync(It.IsAny<FileType>(), It.IsAny<string>()), Times.Never);

            #endregion
        }
        [TestMethod]
        public async Task SourceController_Load_HttpGet_FileFail()
        {
            #region Setups

            _mockItemRepository.SetupGet(c => c.HasInitialized).Returns(false);
            _mockItemRepository.Setup(c => c.Initialize(It.IsAny<List<Item>>()));
            _mockFileConversionManager.Setup(c => c.ConvertToItemAsync(It.IsAny<FileType>(), It.IsAny<string>())).Throws<FileException>();
            _mockHttpRunTimeHelper.Setup(c => c.AppDomainAppPath).Returns("somepath");

            #endregion

            #region Acts

            var result = await _sourceController.Load() as ViewResult;

            #endregion

            #region Asserts

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Model);
            Assert.AreEqual("Index", result.ViewName);
            Assert.AreEqual(_sourceController.LayoutDefault, result.MasterName);
            Assert.IsInstanceOfType(result.Model, typeof(ViewModelBase));

            var viewModel = (ViewModelBase)result.Model;
            Assert.IsNotNull(viewModel);
            Assert.IsNotNull(viewModel.Message);
            Assert.AreEqual("Dosyalar okunurken bir hata oluştu!", viewModel.Message);

            _mockItemRepository.VerifyGet(c => c.HasInitialized, Times.Once);
            _mockItemRepository.Verify(c => c.Initialize(It.IsAny<List<Item>>()), Times.Never);
            _mockFileConversionManager.Verify(c => c.ConvertToItemAsync(It.IsAny<FileType>(), It.IsAny<string>()), Times.AtLeastOnce);

            #endregion
        }
        [TestMethod]
        [ExpectedException(typeof(ControllerException), "Source Controller failed!")]
        public async Task SourceController_Load_HttpGet_GeneralFail()
        {
            #region Setups

            _mockItemRepository.SetupGet(c => c.HasInitialized).Returns(false);
            _mockItemRepository.Setup(c => c.Initialize(It.IsAny<List<Item>>()));
            _mockFileConversionManager.Setup(c => c.ConvertToItemAsync(It.IsAny<FileType>(), It.IsAny<string>())).Throws<Exception>();
            _mockHttpRunTimeHelper.Setup(c => c.AppDomainAppPath).Returns("somepath");

            #endregion

            #region Acts

            var result = await _sourceController.Load() as ViewResult;

            #endregion

            #region Asserts

            Assert.IsNull(result);

            _mockItemRepository.VerifyGet(c => c.HasInitialized, Times.Once);
            _mockItemRepository.Verify(c => c.Initialize(It.IsAny<List<Item>>()), Times.Never);
            _mockFileConversionManager.Verify(c => c.ConvertToItemAsync(It.IsAny<FileType>(), It.IsAny<string>()), Times.AtLeastOnce);

            #endregion
        }

        #endregion
    }
}
