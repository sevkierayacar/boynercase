﻿using Boyner.App.Website.Controllers;
using Boyner.App.Website.Models.Item;
using Boyner.Core.Model.Exceptions;
using Boyner.Core.Model.Interfaces;
using Boyner.Core.Model.Shared;
using Boyner.Test.Base;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Boyner.Test.Contoller
{
    [TestClass]
    public class ItemControllerTest : TestBase
    {
        #region Fields

        private ItemController _itemController;
        private Mock<IItemRepository> _mockItemRepository;

        #endregion

        #region Initialize

        [TestInitialize]
        public void Initialize()
        {
            _mockItemRepository = new Mock<IItemRepository>();
            _itemController = new ItemController(_mockItemRepository.Object);
        }

        #endregion

        #region Tests

        [TestMethod]
        public async Task ItemController_List_HttpGet_Success()
        {
            #region Setups

            _mockItemRepository.Setup(c => c.AllAsync(It.IsAny<Sort>())).ReturnsAsync(base.Items);
            _mockItemRepository.Setup(c => c.FilterAsync(It.IsAny<string>(), It.IsAny<Sort>())).ReturnsAsync(base.Items);

            #endregion

            #region Acts

            var result = await _itemController.List() as ViewResult;

            #endregion

            #region Asserts

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Model);
            Assert.AreEqual("List", result.ViewName);
            Assert.AreEqual(_itemController.LayoutDefault, result.MasterName);
            Assert.IsInstanceOfType(result.Model, typeof(ListViewModel));

            var viewModel = (ListViewModel)result.Model;
            Assert.IsNotNull(viewModel);
            Assert.AreEqual(25, viewModel.Items.Count());
            Assert.IsNull(viewModel.Sort);
            Assert.IsNull(viewModel.Search);

            _mockItemRepository.Verify(c => c.AllAsync(It.IsAny<Sort>()), Times.Once);
            _mockItemRepository.Verify(c => c.FilterAsync(It.IsAny<string>(), It.IsAny<Sort>()), Times.Never);

            #endregion
        }
        [TestMethod]
        public async Task ItemController_List_HttpPost_WithSearchText_Success()
        {
            #region Setups

            _mockItemRepository.Setup(c => c.AllAsync(It.IsAny<Sort>())).ReturnsAsync(base.Items);
            _mockItemRepository.Setup(c => c.FilterAsync(It.IsAny<string>(), It.IsAny<Sort>())).ReturnsAsync(base.Items);

            #endregion

            #region Acts

            var result = await _itemController.List(new ListViewModel
            {
                Search = "something"
            }) as ViewResult;

            #endregion

            #region Asserts

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Model);
            Assert.AreEqual("List", result.ViewName);
            Assert.AreEqual(_itemController.LayoutDefault, result.MasterName);
            Assert.IsInstanceOfType(result.Model, typeof(ListViewModel));

            var viewModel = (ListViewModel)result.Model;
            Assert.IsNotNull(viewModel);
            Assert.AreEqual(25, viewModel.Items.Count());
            Assert.IsNull(viewModel.Sort);
            Assert.IsNotNull(viewModel.Search);
            Assert.IsTrue(!string.IsNullOrWhiteSpace(viewModel.Search));

            _mockItemRepository.Verify(c => c.AllAsync(It.IsAny<Sort>()), Times.Never);
            _mockItemRepository.Verify(c => c.FilterAsync(It.IsAny<string>(), It.IsAny<Sort>()), Times.Once);

            #endregion
        }
        [TestMethod]
        public async Task ItemController_List_HttpPost_WithoutSearchText_Success()
        {
            #region Setups

            _mockItemRepository.Setup(c => c.AllAsync(It.IsAny<Sort>())).ReturnsAsync(base.Items);
            _mockItemRepository.Setup(c => c.FilterAsync(It.IsAny<string>(), It.IsAny<Sort>())).ReturnsAsync(base.Items);

            #endregion

            #region Acts

            var result = await _itemController.List(new ListViewModel
            {
                Search = ""
            }) as ViewResult;

            #endregion

            #region Asserts

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Model);
            Assert.AreEqual("List", result.ViewName);
            Assert.AreEqual(_itemController.LayoutDefault, result.MasterName);
            Assert.IsInstanceOfType(result.Model, typeof(ListViewModel));

            var viewModel = (ListViewModel)result.Model;
            Assert.IsNotNull(viewModel);
            Assert.AreEqual(25, viewModel.Items.Count());
            Assert.IsNull(viewModel.Sort);
            Assert.IsNotNull(viewModel.Search);
            Assert.IsTrue(viewModel.Search == "");

            _mockItemRepository.Verify(c => c.AllAsync(It.IsAny<Sort>()), Times.Once);
            _mockItemRepository.Verify(c => c.FilterAsync(It.IsAny<string>(), It.IsAny<Sort>()), Times.Never);

            #endregion
        }
        [TestMethod]
        public async Task ItemController_List_HttpGet_RepositoryFail()
        {
            #region Setups

            _mockItemRepository.Setup(c => c.AllAsync(It.IsAny<Sort>())).Throws<RepositoryException>();

            #endregion

            #region Acts

            var result = await _itemController.List() as ViewResult;

            #endregion

            #region Asserts

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Model);
            Assert.AreEqual("List", result.ViewName);
            Assert.AreEqual(_itemController.LayoutDefault, result.MasterName);
            Assert.IsInstanceOfType(result.Model, typeof(ListViewModel));

            var viewModel = (ListViewModel)result.Model;
            Assert.IsNotNull(viewModel);
            Assert.IsNotNull(viewModel.Message);
            Assert.AreEqual("Tüm liste alırken hata oluştu!", viewModel.Message);

            _mockItemRepository.Verify(c => c.AllAsync(It.IsAny<Sort>()), Times.Once);

            #endregion
        }
        [TestMethod]
        [ExpectedException(typeof(ControllerException), "Item Controller failed!")]
        public async Task ItemController_List_HttpGet_GeneralFail()
        {
            #region Setups

            _mockItemRepository.Setup(c => c.AllAsync(It.IsAny<Sort>())).Throws<Exception>();

            #endregion

            #region Acts

            var result = await _itemController.List() as ViewResult;

            #endregion

            #region Asserts

            Assert.IsNull(result);

            _mockItemRepository.Verify(c => c.AllAsync(It.IsAny<Sort>()), Times.Once);

            #endregion
        }
        [TestMethod]
        public async Task ItemController_List_HttpPost_RepositoryFail()
        {
            #region Setups

            _mockItemRepository.Setup(c => c.AllAsync(It.IsAny<Sort>())).Throws<RepositoryException>();

            #endregion

            #region Acts

            var result = await _itemController.List(new ListViewModel
            {
                Search = ""
            }) as ViewResult;

            #endregion

            #region Asserts

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Model);
            Assert.AreEqual("List", result.ViewName);
            Assert.AreEqual(_itemController.LayoutDefault, result.MasterName);
            Assert.IsInstanceOfType(result.Model, typeof(ListViewModel));

            var viewModel = (ListViewModel)result.Model;
            Assert.IsNotNull(viewModel);
            Assert.IsNotNull(viewModel.Message);
            Assert.AreEqual("Filtrelenmiş liste alırken hata oluştu!", viewModel.Message);

            _mockItemRepository.Verify(c => c.AllAsync(It.IsAny<Sort>()), Times.Once);

            #endregion
        }
        [TestMethod]
        [ExpectedException(typeof(ControllerException), "Item Controller failed!")]
        public async Task ItemController_List_HttpPost_GeneralFail()
        {
            #region Setups

            _mockItemRepository.Setup(c => c.AllAsync(It.IsAny<Sort>())).Throws<Exception>();

            #endregion

            #region Acts

            var result = await _itemController.List(new ListViewModel
            {
                Search = ""
            }) as ViewResult;

            #endregion

            #region Asserts

            Assert.IsNull(result);

            _mockItemRepository.Verify(c => c.AllAsync(It.IsAny<Sort>()), Times.Once);

            #endregion
        }

        #endregion
    }
}
