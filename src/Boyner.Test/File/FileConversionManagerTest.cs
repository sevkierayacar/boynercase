﻿using Boyner.Core.Model.Enums;
using Boyner.Core.Model.Interfaces;
using Boyner.Logic.File;
using Boyner.Test.Base;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.IO.Abstractions.TestingHelpers;
using System.Threading.Tasks;
using Boyner.Core.Model.Exceptions;

namespace Boyner.Test.File
{
    [TestClass]
    public class FileConversionManagerTest : TestBase
    {
        #region Fields

        private IFileConversionManager _fileConversionManager;
        private MockFile _mockFileXml;
        private MockFile _mockFileJson;
        private MockFile _mockFileCsv;

        #endregion

        #region Initialize

        [TestInitialize]
        public void Initialize()
        {
            _mockFileXml = new MockFile
            {
                FilePath = @"C:\source.xml",
                FileContentExpected = @"<?xml version='1.0' encoding='utf-16'?>
                        <ArrayOfXMLModel
                            xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'
                            xmlns:xsd='http://www.w3.org/2001/XMLSchema'>
                            <XMLModel>
                                <Id>2313960</Id>
                                <VariantId>8645116</VariantId>
                                <GroupId>1369498</GroupId>
                                <Name>Klasik Ceket</Name>
                                <Barcode>DL-I-8-DL-I-5-869987</Barcode>
                                <ListPrice>199.9900</ListPrice>
                                <Properties>G051KS002.P44.L03001 Ceket,Beyaz,50-6N</Properties>
                            </XMLModel>
                        </ArrayOfXMLModel>",
                FileContentNotExpected = @"<?xml version='1.0' encoding='utf-16'?>
                        <Array
                            xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'
                            xmlns:xsd='http://www.w3.org/2001/XMLSchema'>
                        </Array>",
                FileContentNotParsable = "",
            };
            _mockFileJson = new MockFile
            {
                FilePath = @"C:\source.json",
                FileContentExpected = @"[
                                      {
                                        'ProductId': 2313960,
                                        'VariantId': 8645116,
                                        'ColorGroup': 1369498,
                                        'Name': 'Klasik Ceket',
                                        'Barcode': 'DL-I-8-DL-I-5-869987',
                                        'Price': 199.9900,
                                        'Properties': [
                                          'G051KS002.P44.L03001 Ceket',
                                          'Beyaz',
                                          '50-6N'
                                        ]
                                      }
                                    ]",
                FileContentNotExpected = @"<?xml version='1.0' encoding='utf-16'?>
                        <Array
                            xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'
                            xmlns:xsd='http://www.w3.org/2001/XMLSchema'>
                        </Array>",
                FileContentNotParsable = "",
            };
            _mockFileCsv = new MockFile
            {
                FilePath = @"C:\source.csv",
                FileContentExpected = @"2313960;8645116;1369498;Klasik Ceket;DL-I-8-DL-I-5-869987;199.9900;G051KS002.P44.L03001 Ceket,Beyaz,50-6N",
                FileContentNotExpected = @"<?xml version='1.0' encoding='utf-16'?>
                        <Array
                            xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'
                            xmlns:xsd='http://www.w3.org/2001/XMLSchema'>
                        </Array>",
                FileContentNotParsable = "",
            };
        }

        #endregion

        #region Tests

        #region Xml Tests

        [TestMethod]
        public async Task FileConversionManager_FileXml_Load_Success()
        {
            #region Setups

            MockFileSystem mockFileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { _mockFileXml.FilePath, new MockFileData(_mockFileXml.FileContentExpected) }
            });
            _fileConversionManager = new FileConversionManager(mockFileSystem);

            #endregion

            #region Act

            var result = await _fileConversionManager.ConvertToItemAsync(FileType.Xml, _mockFileXml.FilePath);

            #endregion

            #region Asserts

            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(2313960, result[0].ProductId);
            Assert.AreEqual(8645116, result[0].VariantId);
            Assert.AreEqual(1369498, result[0].ColorGroup);
            Assert.AreEqual("Klasik Ceket", result[0].Name);
            Assert.AreEqual("DL-I-8-DL-I-5-869987", result[0].Barcode);
            Assert.AreEqual(199.99M, result[0].Price);
            Assert.IsNotNull(result[0].Properties);
            Assert.AreEqual(3, result[0].Properties.Count);
            Assert.AreEqual("G051KS002.P44.L03001 Ceket", result[0].Properties[0]);
            Assert.AreEqual("Beyaz", result[0].Properties[1]);
            Assert.AreEqual("50-6N", result[0].Properties[2]);

            #endregion
        }
        [TestMethod]
        [ExpectedException(typeof(FileException), "Expected list node is not found!")]
        public async Task FileConversionManager_FileXml_Load_Fail_Expect_NotExpected()
        {
            #region Setups

            MockFileSystem mockFileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { _mockFileXml.FilePath, new MockFileData(_mockFileXml.FileContentNotExpected) }
            });
            _fileConversionManager = new FileConversionManager(mockFileSystem);

            #endregion

            #region Act

            await _fileConversionManager.ConvertToItemAsync(FileType.Xml, _mockFileXml.FilePath);

            #endregion
        }
        [TestMethod]
        [ExpectedException(typeof(FileException), "Xml file cannot be parsed!")]
        public async Task FileConversionManager_FileXml_Load_Fail_Expect_NotParsable()
        {
            #region Setups

            MockFileSystem mockFileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { _mockFileXml.FilePath, new MockFileData(_mockFileXml.FileContentNotParsable) }
            });
            _fileConversionManager = new FileConversionManager(mockFileSystem);

            #endregion

            #region Act

            await _fileConversionManager.ConvertToItemAsync(FileType.Xml, _mockFileXml.FilePath);

            #endregion
        }

        #endregion

        #region Json Tests

        [TestMethod]
        public async Task FileConversionManager_FileJson_Load_Success()
        {
            #region Setups

            MockFileSystem mockFileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { _mockFileJson.FilePath, new MockFileData(_mockFileJson.FileContentExpected) }
            });
            _fileConversionManager = new FileConversionManager(mockFileSystem);

            #endregion

            #region Act

            var result = await _fileConversionManager.ConvertToItemAsync(FileType.Json, _mockFileJson.FilePath);

            #endregion

            #region Asserts

            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(2313960, result[0].ProductId);
            Assert.AreEqual(8645116, result[0].VariantId);
            Assert.AreEqual(1369498, result[0].ColorGroup);
            Assert.AreEqual("Klasik Ceket", result[0].Name);
            Assert.AreEqual("DL-I-8-DL-I-5-869987", result[0].Barcode);
            Assert.AreEqual(199.99M, result[0].Price);
            Assert.IsNotNull(result[0].Properties);
            Assert.AreEqual(3, result[0].Properties.Count);
            Assert.AreEqual("G051KS002.P44.L03001 Ceket", result[0].Properties[0]);
            Assert.AreEqual("Beyaz", result[0].Properties[1]);
            Assert.AreEqual("50-6N", result[0].Properties[2]);

            #endregion
        }
        [TestMethod]
        [ExpectedException(typeof(FileException), "File parse error!")]
        public async Task FileConversionManager_FileJson_Load_Fail_Expect_NotExpected()
        {
            #region Setups

            MockFileSystem mockFileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { _mockFileJson.FilePath, new MockFileData(_mockFileJson.FileContentNotExpected) }
            });
            _fileConversionManager = new FileConversionManager(mockFileSystem);

            #endregion

            #region Act

            await _fileConversionManager.ConvertToItemAsync(FileType.Json, _mockFileJson.FilePath);

            #endregion
        }
        [TestMethod]
        [ExpectedException(typeof(FileException), "File parse error!")]
        public async Task FileConversionManager_FileJson_Load_Fail_Expect_NotParsable()
        {
            #region Setups

            MockFileSystem mockFileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { _mockFileJson.FilePath, new MockFileData(_mockFileJson.FileContentNotParsable) }
            });
            _fileConversionManager = new FileConversionManager(mockFileSystem);

            #endregion

            #region Act

            await _fileConversionManager.ConvertToItemAsync(FileType.Json, _mockFileJson.FilePath);

            #endregion
        }

        #endregion

        #region Csv Tests

        [TestMethod]
        public async Task FileConversionManager_FileCsv_Load_Success()
        {
            #region Setups

            MockFileSystem mockFileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { _mockFileCsv.FilePath, new MockFileData(_mockFileCsv.FileContentExpected) }
            });
            _fileConversionManager = new FileConversionManager(mockFileSystem);

            #endregion

            #region Act

            var result = await _fileConversionManager.ConvertToItemAsync(FileType.Csv, _mockFileCsv.FilePath);

            #endregion

            #region Asserts

            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(2313960, result[0].ProductId);
            Assert.AreEqual(8645116, result[0].VariantId);
            Assert.AreEqual(1369498, result[0].ColorGroup);
            Assert.AreEqual("Klasik Ceket", result[0].Name);
            Assert.AreEqual("DL-I-8-DL-I-5-869987", result[0].Barcode);
            Assert.AreEqual(199.99M, result[0].Price);
            Assert.IsNotNull(result[0].Properties);
            Assert.AreEqual(3, result[0].Properties.Count);
            Assert.AreEqual("G051KS002.P44.L03001 Ceket", result[0].Properties[0]);
            Assert.AreEqual("Beyaz", result[0].Properties[1]);
            Assert.AreEqual("50-6N", result[0].Properties[2]);

            #endregion
        }
        [TestMethod]
        [ExpectedException(typeof(FileException), "File parse error!")]
        public async Task FileConversionManager_FileCsv_Load_Fail_Expect_NotExpected()
        {
            #region Setups

            MockFileSystem mockFileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { _mockFileCsv.FilePath, new MockFileData(_mockFileCsv.FileContentNotExpected) }
            });
            _fileConversionManager = new FileConversionManager(mockFileSystem);

            #endregion

            #region Act

            await _fileConversionManager.ConvertToItemAsync(FileType.Csv, _mockFileCsv.FilePath);

            #endregion
        }
        [TestMethod]
        [ExpectedException(typeof(FileException), "File parse error!")]
        public async Task FileConversionManager_FileCsv_Load_Fail_Expect_NotParsable()
        {
            #region Setups

            MockFileSystem mockFileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { _mockFileCsv.FilePath, new MockFileData(_mockFileCsv.FileContentNotParsable) }
            });
            _fileConversionManager = new FileConversionManager(mockFileSystem);

            #endregion

            #region Act

            await _fileConversionManager.ConvertToItemAsync(FileType.Csv, _mockFileCsv.FilePath);

            #endregion
        }

        #endregion

        #endregion
    }

    internal class MockFile
    {
        public string FilePath { get; set; }
        public string FileContentExpected { get; set; }
        public string FileContentNotExpected{ get; set; }
        public string FileContentNotParsable{ get; set; }
    }
}