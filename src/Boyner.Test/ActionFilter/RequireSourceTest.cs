﻿using Boyner.App.Website.AppCode.ActionFilters;
using Boyner.Core.Model.Interfaces;
using Boyner.Test.Base;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Web.Mvc;
using System.Linq;

namespace Boyner.Test.ActionFilter
{
    [TestClass]
    public class RequireSourceTest : TestBase
    {
        #region Fields

        private RequireSource _requireSource;
        private Mock<IItemRepository> _mockItemRepository;

        #endregion

        #region Initialize

        [TestInitialize]
        public void Initialize()
        {
            _mockItemRepository = new Mock<IItemRepository>();
            _requireSource = new RequireSource(_mockItemRepository.Object);
        }

        #endregion

        #region Tests

        [TestMethod]
        public void RequireSource_WhenFilesNotLoaded()
        {
            #region Setups

            _mockItemRepository.SetupGet(c => c.HasInitialized).Returns(false);

            #endregion

            #region Acts

            var actionExecutingContext = new ActionExecutingContext();
            _requireSource.OnActionExecuting(actionExecutingContext);

            #endregion

            #region Asserts

            Assert.IsNotNull(actionExecutingContext);
            Assert.IsNotNull(actionExecutingContext.Result);
            Assert.AreEqual(typeof(RedirectToRouteResult), actionExecutingContext.Result.GetType());
            Assert.AreEqual("Source", ((RedirectToRouteResult)actionExecutingContext.Result).RouteValues.FirstOrDefault(c => c.Key == "controller").Value);
            Assert.AreEqual("Load", ((RedirectToRouteResult)actionExecutingContext.Result).RouteValues.FirstOrDefault(c => c.Key == "action").Value);

            #endregion
        }
        [TestMethod]
        public void RequireSource_WhenFilesLoaded()
        {
            #region Setups

            _mockItemRepository.SetupGet(c => c.HasInitialized).Returns(true);

            #endregion

            #region Acts

            var actionExecutingContext = new ActionExecutingContext();
            _requireSource.OnActionExecuting(actionExecutingContext);

            #endregion

            #region Asserts

            Assert.IsNotNull(actionExecutingContext);
            Assert.IsNull(actionExecutingContext.Result);

            #endregion
        }

        #endregion
    }
}
