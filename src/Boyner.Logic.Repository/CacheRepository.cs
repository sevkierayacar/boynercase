﻿using Boyner.Core.Model.Interfaces;
using System;
using System.Runtime.Caching;

namespace Boyner.Logic.Repository
{
    public class CacheRepository<T> : ICacheRepository<T> where T : class, new()
    {
        #region Fields
        
        private static readonly ObjectCache Cache = MemoryCache.Default;
        private readonly string _cacheName;

        #endregion

        #region Properties - ICacheRepository

        public T CacheObject => (T)Cache[_cacheName];
        public bool HasCache => Cache.Contains(_cacheName) && Cache[_cacheName] != null;

        #endregion

        #region Constructors

        public CacheRepository()
        {
            _cacheName = typeof(T).ToString();
        }

        #endregion

        #region Methods - Public - ICacheRepository

        public void Add(T entity)
        {
            Cache.Add(_cacheName, entity, DateTimeOffset.MaxValue);
        }
        public void Remove()
        {
            Cache.Remove(_cacheName);
        }

        #endregion
    }
}
