﻿using Boyner.Core.Model.Entities;
using Boyner.Core.Model.Enums;
using Boyner.Core.Model.Exceptions;
using Boyner.Core.Model.Extensions;
using Boyner.Core.Model.Helpers;
using Boyner.Core.Model.Interfaces;
using Boyner.Core.Model.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Boyner.Core.Logging;

namespace Boyner.Logic.Repository
{
    public class ItemRepository : IItemRepository
    {
        #region Fields

        private static readonly object LockObject = new object();
        private readonly ICacheRepository<List<Item>> _cacheRepository;

        #endregion

        #region Properties - IItemRepository

        public bool HasInitialized => _cacheRepository.HasCache;

        #endregion

        #region Constructors

        public ItemRepository(ICacheRepository<List<Item>> cacheRepository)
        {
            _cacheRepository = cacheRepository;
        }

        #endregion

        #region Methods - Public - IItemRepository

        public void Initialize(List<Item> items)
        {
            lock (LockObject)
            {
                if (_cacheRepository.HasCache)
                {
                    lock (LockObject)
                    {
                        _cacheRepository.Remove();
                        SharpLogger.LogDebug("Cache is removed!");
                    }
                }

                _cacheRepository.Add(items);
                SharpLogger.LogDebug("Cache is added!");

                if (!_cacheRepository.HasCache) //Check if everything when as expected!
                {
                    throw new RepositoryException("Cache is not filled!");
                }
            }
            SharpLogger.LogProcessInfo("Cache is initlized!");
        }
        //IMPORTANT!!!: Even though the source is on cache we used async. Because if it were from database we'd have used like this
        public async Task<ICollection<Item>> AllAsync(Sort sort = null)
        {
            SharpLogger.LogProcessInfo("Request started!", sort != null ? new { sort.SortingDirectionType, sort.SortingField } : null);

            return await Task.Run(() =>
            {
                if (!_cacheRepository.HasCache)
                {
                    throw new RepositoryException("Items have not been loaded yet!");
                }

                var result = _cacheRepository.CacheObject.AsQueryable();

                if (sort != null && sort.IsValid())
                    return result.OrderByFieldName(sort.SortingField, sort.SortingDirectionType).ToList();

                return result.ToList();
            } );
        }
        //IMPORTANT!!!: Even though the source is on cache we used async. Because if it were from database we'd have used like this
        public async Task<ICollection<Item>> FilterAsync(string searchText, Sort sort = null)
        {
            SharpLogger.LogProcessInfo("Request started!", sort != null ? new { sort.SortingDirectionType, sort.SortingField } : null);
            SharpLogger.LogProcessInfo("Search Text", new { searchText } );

            if (!_cacheRepository.HasCache)
            {
                throw new RepositoryException("Items have not been loaded yet!");
            }

            return await Task.Run(() =>
            {
                #region Separating Keywords into Managable object

                List<SearchKeyword> searchKeywords = Regex.Matches(searchText, @"[\""].+?[\""]|[^ ]+")
                    .Cast<Match>()
                    .Select(c => new SearchKeyword
                    {
                        Keyword = ClearKeyword(c.Value),
                        SearchType = GetSeatchType(c.Value.Trim())
                    })
                    .ToList();

                SharpLogger.LogDebug("searchKeywords: ", searchKeywords.Select(c => new { c.SearchType, c.Keyword }));

                #endregion

                #region Building Search Linq Predicate

                //IMPORTANT: The order of these foreaches are vital! OR's must be earlier than the AND's. This is because how the predicate is build!

                Expression<Func<Item, bool>> predicate = null;

                if (searchKeywords.All(c => c.SearchType != SearchType.MustContain)) //When there is a MustContain, the search must focus MustContain
                {
                    foreach (var searchKeyword in searchKeywords.Where(c => c.SearchType == SearchType.MayContain))
                    {
                        if (predicate == null) predicate = PredicateBuilder.False<Item>();
                        string temp = searchKeyword.Keyword;
                        predicate = predicate.Or(SearchCriteriasWithContains(temp));
                    }
                }
                foreach (var searchKeyword in searchKeywords.Where(c => c.SearchType == SearchType.AllContain))
                {
                    if (predicate == null) predicate = PredicateBuilder.True<Item>();
                    string temp = searchKeyword.Keyword;
                    foreach (var tmp in temp.Split(' '))
                    {
                        predicate = predicate.And(SearchCriteriasWithContains(tmp));
                    }
                }
                foreach (var searchKeyword in searchKeywords.Where(c => c.SearchType == SearchType.MustContain))
                {
                    if (predicate == null) predicate = PredicateBuilder.True<Item>();
                    string temp = searchKeyword.Keyword;
                    predicate = predicate.And(SearchCriteriasWithContains(temp));
                }
                foreach (var searchKeyword in searchKeywords.Where(c => c.SearchType == SearchType.NotContain))
                {
                    if (predicate == null) predicate = PredicateBuilder.True<Item>();
                    string temp = searchKeyword.Keyword;
                    predicate = predicate.And(SearchCriteriasWithNotContains(temp));
                }

                var result = _cacheRepository.CacheObject.AsQueryable().Where(predicate ?? PredicateBuilder.False<Item>());

                #endregion

                #region Sorting when Field is Specificly Requested

                if (sort != null && sort.IsValid())
                    return result.OrderByFieldName(sort.SortingField, sort.SortingDirectionType).ToList();

                #endregion

                #region Sorting by requested Search Keyword

                var sortingKeyword = searchKeywords.FirstOrDefault()?.Keyword;

                return result
                    .OrderByDescending(c => Regex.Matches(c.Name, $@"[\W^]*{sortingKeyword}[\W$]*", RegexOptions.IgnoreCase).Count)
                    .ThenByDescending(c => Regex.Matches(string.Join("|", c.Properties), $@"[\W^]*{sortingKeyword}[\W$]*", RegexOptions.IgnoreCase).Count)
                    .ToList();

                #endregion
            });
        }

        #endregion

        #region Methods - Private

        private Expression<Func<Item, bool>> SearchCriteriasWithContains(string searchText)
        {
            return c =>
                    c.Name.IndexOf(searchText, StringComparison.OrdinalIgnoreCase) >= 0
                    || c.Properties.Any(p => p.IndexOf(searchText, StringComparison.OrdinalIgnoreCase) >= 0)
                ;
        }
        private Expression<Func<Item, bool>> SearchCriteriasWithNotContains(string searchText)
        {
            return c =>
                    c.Name.IndexOf(searchText, StringComparison.OrdinalIgnoreCase) < 0
                    && c.Properties.All(p => p.IndexOf(searchText, StringComparison.OrdinalIgnoreCase) < 0)

                    //These can also be added
                    //&& c.ProductId.ToString().IndexOf(searchText, StringComparison.OrdinalIgnoreCase) < 0
                    //&& c.VariantId.ToString().IndexOf(searchText, StringComparison.OrdinalIgnoreCase) < 0
                    //&& c.ColorGroup.ToString().IndexOf(searchText, StringComparison.OrdinalIgnoreCase) < 0
                    //&& c.Barcode.IndexOf(searchText, StringComparison.OrdinalIgnoreCase) < 0
                    //&& c.Price.ToString().IndexOf(searchText, StringComparison.OrdinalIgnoreCase) < 0
                ;
        }
        private string ClearKeyword(string keyword)
        {
            keyword = keyword.Trim();

            switch (GetSeatchType(keyword))
            {
                case SearchType.AllContain:
                    keyword = keyword.Replace("\"", "");
                    break;
                case SearchType.MustContain:
                case SearchType.NotContain:
                    keyword = keyword.Substring(0, keyword.Length - 1);
                    break;
            }

            return keyword;
        }
        private SearchType GetSeatchType(string keyword)
        {
            var lastChar = keyword[keyword.Length - 1];
            switch (lastChar)
            {
                case '-': return SearchType.NotContain;
                case '+': return SearchType.MustContain;
                case '"': return SearchType.AllContain;
                default: return SearchType.MayContain;
            }
        }

        #endregion
    }
}