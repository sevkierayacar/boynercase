﻿using System;

namespace Boyner.Core.Model.Exceptions
{
    [Serializable]
    public class RepositoryException : Exception
    {
        #region Contructors

        public RepositoryException()
        {
        }
        public RepositoryException(string message) : base(message)
        {
        }
        public RepositoryException(string message, Exception innerException) : base(message, innerException)
        {
        }

        #endregion
    }
}