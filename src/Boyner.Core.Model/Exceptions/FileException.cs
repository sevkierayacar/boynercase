﻿using System;

namespace Boyner.Core.Model.Exceptions
{
    [Serializable]
    public class FileException : Exception
    {
        #region Contructors

        public FileException()
        {
        }
        public FileException(string message) : base(message)
        {
        }
        public FileException(string message, Exception innerException) : base(message, innerException)
        {
        }

        #endregion
    }
}