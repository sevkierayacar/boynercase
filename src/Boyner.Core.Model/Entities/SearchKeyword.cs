﻿using Boyner.Core.Model.Enums;

namespace Boyner.Core.Model.Entities
{
    public class SearchKeyword
    {
        public string Keyword { get; set; }
        public SearchType SearchType { get; set; }
    }
}