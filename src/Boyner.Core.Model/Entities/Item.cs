﻿using System.Collections.Generic;

namespace Boyner.Core.Model.Entities
{
    public class Item
    {
        #region Properties

        public long ProductId { get; set; }
        public long VariantId { get; set; }
        public long ColorGroup { get; set; }
        public string Name { get; set; }
        public string Barcode { get; set; }
        public decimal Price { get; set; }
        public List<string> Properties { get; set; }

        #endregion

        #region Constructors

        public Item()
        {
            Properties = new List<string>();
        }

        #endregion
    }
}