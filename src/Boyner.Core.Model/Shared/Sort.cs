﻿using Boyner.Core.Model.Enums;

namespace Boyner.Core.Model.Shared
{
    public class Sort
    {
        #region Properties

        public SortingDirectionType SortingDirectionType { get; set; }
        public string SortingField { get; set; }

        #endregion

        #region Methods - Public

        public bool IsValid()
        {
            return !string.IsNullOrEmpty(SortingField);
        }

        #endregion
    }
}