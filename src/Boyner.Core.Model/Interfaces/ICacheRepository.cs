﻿namespace Boyner.Core.Model.Interfaces
{
    public interface ICacheRepository<T> : INinject
    {
        T CacheObject { get; }
        bool HasCache { get; }

        void Add(T entity);
        void Remove();
    }
}