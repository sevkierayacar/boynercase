﻿using Boyner.Core.Model.Entities;
using Boyner.Core.Model.Shared;
using System.Collections.Generic;
using System.Threading.Tasks;
using Boyner.Core.Model.Exceptions;

namespace Boyner.Core.Model.Interfaces
{
    public interface IItemRepository : INinject
    {
        bool HasInitialized { get; }

        /// <summary>
        /// Inserts given items into cache source.
        /// </summary>
        /// <exception cref="RepositoryException"></exception>
        /// <param name="items"></param>
        /// <returns></returns>
        void Initialize(List<Item> items);

        /// <summary>
        /// Gets all existing records at once
        /// </summary>
        /// <exception cref="RepositoryException"></exception>
        /// <param name="sort"></param>
        /// <returns></returns>
        Task<ICollection<Item>> AllAsync(Sort sort = null);

        /// <summary>
        /// Filters the records with given options
        /// </summary>
        /// <exception cref="RepositoryException"></exception>
        /// <param name="searchText"></param>
        /// <param name="sort"></param>
        /// <returns></returns>
        Task<ICollection<Item>> FilterAsync(string searchText, Sort sort = null);
    }
}