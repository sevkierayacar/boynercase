﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Boyner.Core.Model.Entities;
using Boyner.Core.Model.Enums;
using Boyner.Core.Model.Exceptions;

namespace Boyner.Core.Model.Interfaces
{
    public interface IFileConversionManager : INinject
    {
        /// <summary>
        /// Reads from a file source and converts them into <code>Item</code> object
        /// </summary>
        /// <exception cref="FileException"></exception>
        /// <param name="fileType"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        Task<List<Item>> ConvertToItemAsync(FileType fileType, string filePath);
    }
}