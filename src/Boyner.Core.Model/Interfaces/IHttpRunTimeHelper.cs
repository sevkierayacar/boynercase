﻿namespace Boyner.Core.Model.Interfaces
{
    public interface IHttpRunTimeHelper : INinject
    {
        string AppDomainAppPath { get; }
    }
}