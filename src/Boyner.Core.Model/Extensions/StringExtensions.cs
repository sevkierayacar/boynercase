﻿namespace Boyner.Core.Model.Extensions
{
    public static class StringExtensions
    {
        public static string FixInvalidTurkishChars(this string value)
        {
            return value
                    .Replace("Ý", "İ")
                    .Replace("ð", "ğ")
                    .Replace("ý", "ı")
                ;
        }
    }
}