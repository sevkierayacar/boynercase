﻿using Boyner.Core.Model.Enums;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Boyner.Core.Model.Extensions
{
    public static class LinqExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="orderByField"></param>
        /// <param name="sortingDirectionType"></param>
        /// <returns></returns>
        public static IQueryable<T> OrderByFieldName<T>(this IQueryable<T> source, string orderByField, SortingDirectionType sortingDirectionType = SortingDirectionType.Ascending)
        {
            string command = sortingDirectionType == SortingDirectionType.Descending ? "OrderByDescending" : "OrderBy";
            var type = typeof(T);

            if (string.IsNullOrWhiteSpace(orderByField))
                throw new ArgumentException("orderByField must be given a value!");

            var property = type.GetProperty(orderByField);
            var parameter = Expression.Parameter(type, "p");
            if (property == null)
                throw new ArgumentNullException("Property is null and cannot be generated!");


            var propertyAccess = Expression.MakeMemberAccess(parameter, property);
            var orderByExpression = Expression.Lambda(propertyAccess, parameter);
            var resultExpression = Expression.Call(typeof(Queryable), command, new Type[] { type, property.PropertyType },
                source.Expression, Expression.Quote(orderByExpression));
            return source.Provider.CreateQuery<T>(resultExpression);
        }
    }
}