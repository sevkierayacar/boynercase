﻿using System.ComponentModel;

namespace Boyner.Core.Model.Extensions
{
    public static class ConversionExceptions
    {
        #region Methods - Public

        public static TConvertionType Convert<TConvertionType>(this object value, TConvertionType defaultValue = default(TConvertionType))
        {
            try
            {
                var converter = TypeDescriptor.GetConverter(typeof(TConvertionType));
                return (TConvertionType)converter.ConvertFromInvariantString(value.ToString());
            }
            catch
            {
                return defaultValue;
            }
        }

        #endregion
    }
}