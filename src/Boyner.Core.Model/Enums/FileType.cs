﻿namespace Boyner.Core.Model.Enums
{
    public enum FileType
    {
        Json,
        Csv,
        Xml
    }
}