﻿namespace Boyner.Core.Model.Enums
{
    public enum SortingDirectionType
    {
        Ascending,
        Descending
    }
}