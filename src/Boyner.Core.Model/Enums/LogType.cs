﻿namespace Boyner.Core.Model.Enums
{
    public enum LogType
    {
        Error,
        ProcessInfo,
        Warning,
        Debug
    }
}