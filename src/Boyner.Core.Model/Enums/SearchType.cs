﻿namespace Boyner.Core.Model.Enums
{
    public enum SearchType
    {
        /// <summary>
        /// All keywords must exist within a record
        /// </summary>
        /// <example>Usage: "beyaz gömlek"</example>
        AllContain = 3,

        /// <summary>
        /// Separated by empty space and at least 1 of them should be found in a record
        /// </summary>
        /// <example>Usage: beyaz gömlek</example>
        MayContain = 4,

        /// <summary>
        /// Separated by empty space and at least the one with plus should be found in a record
        /// </summary>
        /// <example>Usage: beyaz gömlek+</example>
        MustContain = 2,
        NotContain = 1
    }
}