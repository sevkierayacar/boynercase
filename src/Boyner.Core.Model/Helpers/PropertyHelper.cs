﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace Boyner.Core.Model.Helpers
{
    public static class PropertyHelper
    {
        public static string GetPropertyName<TPropertySource>(Expression<Func<TPropertySource, object>> expression)
        {
            var lambda = expression as LambdaExpression;
            MemberExpression memberExpression;
            if (lambda.Body is UnaryExpression)
            {
                var unaryExpression = (UnaryExpression)lambda.Body;
                memberExpression = unaryExpression.Operand as MemberExpression;
            }
            else
            {
                memberExpression = lambda.Body as MemberExpression;
            }

            if (memberExpression == null)
                throw new ArgumentException("Please provide a lambda expression like 'n => n.PropertyName'");

            var propertyInfo = memberExpression.Member as PropertyInfo;

            if (propertyInfo == null)
                throw new NullReferenceException("propertyInfo cannot be null");

            return propertyInfo.Name;
        }
    }
}