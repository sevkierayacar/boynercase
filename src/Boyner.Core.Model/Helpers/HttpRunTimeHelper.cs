﻿using Boyner.Core.Model.Interfaces;
using System.Web;

namespace Boyner.Core.Model.Helpers
{
    public class HttpRunTimeHelper : IHttpRunTimeHelper
    {
        public string AppDomainAppPath => HttpRuntime.AppDomainAppPath;
    }
}